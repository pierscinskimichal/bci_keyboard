class Settings:
    def __init__(self):
        self.COLOR_GREEN = [0, 100, 15]
        self.COLOR_BLACK = [0, 0, 0]
        self.COLOR_WHITE = [255, 255, 255]

        self.WIDTH = 1400
        self.HEIGTH = 720

        self.PADDING_RIGHT = 20
        self.PADDING_BOTTOM = 20

        self.RECT_WIDTH = self.WIDTH/2 - self.PADDING_RIGHT
        self.RECT_HEIGTH = self.HEIGTH/2 - self.PADDING_BOTTOM        

        self._padding_left_4x4 = 100
        self._padding_right_4x4 = 20
        self._padding_top_4x4 = 25
        self._padding_bottom_4x4 = 25

        self._padding_left_2x2 = 150
        self._padding_right_2x2 = 20
        self._padding_top_2x2 = 25
        self._padding_bottom_2x2 = 25

        self._horizontal_gap_4x4 = (self.RECT_WIDTH - self._padding_left_4x4 - self._padding_right_4x4)/4
        self._vertical_gap_4x4 = (self.RECT_HEIGTH - self._padding_top_4x4 - self._padding_bottom_4x4)/4
        
        self._horizontal_gap_2x2 = (self.RECT_WIDTH - self._padding_left_4x4 - self._padding_right_4x4)/2
        self._vertical_gap_2x2 = (self.RECT_HEIGTH - self._padding_top_4x4 - self._padding_bottom_4x4)/2

        self.FRAMES_PER_SECOND = 16

        self.TOP_LEFT_FREQUENCY = self.FRAMES_PER_SECOND / 1
        self.TOP_RIGHT_FREQUENCY = self.FRAMES_PER_SECOND / 2
        self.BOTTOM_LEFT_FREQUENCY = self.FRAMES_PER_SECOND / 4
        self.BOTTOM_RIGHT_FREQUENCY = self.FRAMES_PER_SECOND / 16
