class Renderer_4x4:
    def __init__(self, chars, settings):
        
        self._top_left_4x4 = chars._top_left_4x4
        self._top_right_4x4 = chars._top_right_4x4
        self._bottom_left_4x4 = chars._bottom_left_4x4
        self._bottom_right_4x4 = chars._bottom_right_4x4

        self._charmap = {}
        self._charmap[0] = self._top_left_4x4
        self._charmap[1] = self._top_right_4x4
        self._charmap[2] = self._bottom_left_4x4
        self._charmap[3] = self._bottom_right_4x4

        self._prerendered_4x4 = chars._prerendered_4x4

        self._padding_left_4x4 = settings._padding_left_4x4
        self._padding_top_4x4 = settings._padding_top_4x4
        self._horizontal_gap_4x4 = settings._horizontal_gap_4x4
        self._vertical_gap_4x4 = settings._vertical_gap_4x4

    def fill_surface(self, surface, quarter):
        for row in range(0, 4):
            for column in range(0, 4):
                surface.blit(
                    self._prerendered_4x4[self._charmap[quarter][row][column]],
                    (
                        0 + self._padding_left_4x4 + column * self._horizontal_gap_4x4,
                        0 + self._padding_top_4x4 + row * self._vertical_gap_4x4
                    )
                )

    def get_chars_by_quarter(self, quarter):
        return self._charmap[quarter]



