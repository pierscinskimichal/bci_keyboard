class Renderer_1x1:
    def __init__(self, chars, matrix_2x2):
        
        self._prerendered_1x1 = chars._prerendered_1x1

        self._charmap = {}
        self._charmap[0] = matrix_2x2[0][0]
        self._charmap[1] = matrix_2x2[0][1]
        self._charmap[2] = matrix_2x2[1][0]
        self._charmap[3] = matrix_2x2[1][1]

    def fill_surface(self, surface, quarter):
        text = self._prerendered_1x1[self._charmap[quarter]]
        text_rect = text.get_rect(center=((surface.get_width())/2, (surface.get_height())/2))
        surface.blit(text, text_rect)
