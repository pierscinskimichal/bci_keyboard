# -*- coding: utf-8 -*-
"""
Created on Sun Jan  6 18:26:54 2019

@author: piers
"""

import pygame
import sys
import datetime

from chars import Chars
from settings import Settings
from renderer_4x4 import Renderer_4x4
from renderer_2x2 import Renderer_2x2
from renderer_1x1 import Renderer_1x1

from pygame.locals import *

class App:
    def __init__(self):
        self._running = True
        self._display_surf = None
        self._counter = 0
        self._clock = pygame.time.Clock()

        self._state = 0
        self._keys = {}
        self._keys[pygame.K_0] = 0
        self._keys[pygame.K_1] = 1
        self._keys[pygame.K_2] = 2
        self._keys[pygame.K_3] = 3
        
        self._top_left_display = False
        self._top_right_display = False
        self._bottom_left_display = False
        self._bottom_right_display = False

        self._settings = Settings()
        self._chars = Chars(self._settings)

        self.TOP_LEFT_RECT = pygame.Rect(0, 0, self._settings.RECT_WIDTH - self._settings.PADDING_RIGHT, self._settings.RECT_HEIGTH - self._settings.PADDING_BOTTOM)
        self.TOP_RIGHT_RECT = pygame.Rect(self._settings.RECT_WIDTH + self._settings.PADDING_RIGHT, 0, self._settings.RECT_WIDTH - self._settings.PADDING_RIGHT, self._settings.RECT_HEIGTH - self._settings.PADDING_BOTTOM)
        self.BOTTOM_LEFT_RECT = pygame.Rect(0, self._settings.RECT_HEIGTH + self._settings.PADDING_BOTTOM, self._settings.RECT_WIDTH - self._settings.PADDING_RIGHT, self._settings.RECT_HEIGTH - self._settings.PADDING_BOTTOM)
        self.BOTTOM_RIGHT_RECT = pygame.Rect(self._settings.RECT_WIDTH + self._settings.PADDING_RIGHT, self._settings.RECT_HEIGTH + self._settings.PADDING_BOTTOM, self._settings.RECT_WIDTH - self._settings.PADDING_RIGHT, self._settings.RECT_HEIGTH - self._settings.PADDING_BOTTOM)

        self._top_left_surface_light = pygame.Surface((self.TOP_LEFT_RECT.width, self.TOP_LEFT_RECT.height))
        self._top_left_surface_dark = pygame.Surface((self.TOP_LEFT_RECT.width, self.TOP_LEFT_RECT.height))

        self._top_right_surface_light = pygame.Surface((self.TOP_LEFT_RECT.width, self.TOP_LEFT_RECT.height))
        self._top_right_surface_dark = pygame.Surface((self.TOP_LEFT_RECT.width, self.TOP_LEFT_RECT.height))

        self._bottom_left_surface_light = pygame.Surface((self.TOP_LEFT_RECT.width, self.TOP_LEFT_RECT.height))
        self._bottom_left_surface_dark = pygame.Surface((self.TOP_LEFT_RECT.width, self.TOP_LEFT_RECT.height))

        self._bottom_right_surface_light = pygame.Surface((self.TOP_LEFT_RECT.width, self.TOP_LEFT_RECT.height))
        self._bottom_right_surface_dark = pygame.Surface((self.TOP_LEFT_RECT.width, self.TOP_LEFT_RECT.height))

        self._renderer = Renderer_4x4(self._chars, self._settings)
        self.render_surfaces()

    def render_surfaces(self):
        self._top_left_surface_light.fill(self._settings.COLOR_GREEN)
        self._top_left_surface_dark.fill(self._settings.COLOR_BLACK)
        self._top_right_surface_light.fill(self._settings.COLOR_GREEN)
        self._top_right_surface_dark.fill(self._settings.COLOR_BLACK)
        self._bottom_left_surface_light.fill(self._settings.COLOR_GREEN)
        self._bottom_left_surface_dark.fill(self._settings.COLOR_BLACK)
        self._bottom_right_surface_light.fill(self._settings.COLOR_GREEN)
        self._bottom_right_surface_dark.fill(self._settings.COLOR_BLACK)

        self._renderer.fill_surface(self._top_left_surface_light, 0)
        self._renderer.fill_surface(self._top_left_surface_dark, 0)
        self._renderer.fill_surface(self._top_right_surface_light, 1)
        self._renderer.fill_surface(self._top_right_surface_dark, 1)
        self._renderer.fill_surface(self._bottom_left_surface_light, 2)
        self._renderer.fill_surface(self._bottom_left_surface_dark, 2)
        self._renderer.fill_surface(self._bottom_right_surface_light, 3)
        self._renderer.fill_surface(self._bottom_right_surface_dark, 3)

    def on_init(self):
        pygame.init()
        self._display_surf = pygame.display.set_mode([self._settings.WIDTH, self._settings.HEIGTH])
        self._display_surf.fill(self._settings.COLOR_BLACK)
        self._running = True

    def on_event(self, event):
        if event.type == pygame.QUIT:
            self._running = False

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_0 or event.key == pygame.K_1 or event.key == pygame.K_2 or event.key == pygame.K_3:
                self._state = (self._state + 1) % 3

                if self._state == 0:
                    self._renderer = Renderer_4x4(self._chars, self._settings)
                elif self._state == 1:
                    self._renderer = Renderer_2x2(self._chars, self._settings, self._renderer.get_chars_by_quarter(self._keys[event.key]))
                elif self._state == 2:
                    self._renderer = Renderer_1x1(self._chars, self._renderer.get_chars_by_quarter(self._keys[event.key]))
                else:
                    pass
                
                self.render_surfaces()
                self._display_surf.blit(self._top_left_surface_light, (0, 0))
                self._display_surf.blit(self._top_right_surface_light, (self.TOP_RIGHT_RECT.x, self.TOP_RIGHT_RECT.y))
                self._display_surf.blit(self._bottom_left_surface_light, (self.BOTTOM_LEFT_RECT.x, self.BOTTOM_LEFT_RECT.y))
                self._display_surf.blit(self._bottom_right_surface_light, (self.BOTTOM_RIGHT_RECT.x, self.BOTTOM_RIGHT_RECT.y))
                pygame.display.update([self.TOP_LEFT_RECT, self.TOP_RIGHT_RECT, self.BOTTOM_LEFT_RECT, self.BOTTOM_RIGHT_RECT])            

    def on_loop(self):
        self._counter += 1
        if self._counter == 4096:
            self._counter = 0

    def on_render(self):

        rects_to_be_rendered = []

        if self._counter % self._settings.TOP_LEFT_FREQUENCY == 0:

            if self._top_left_display:
                self._display_surf.blit(self._top_left_surface_light, (0, 0))
            else:
                self._display_surf.blit(self._top_left_surface_dark, (0, 0))

            rects_to_be_rendered.append(self.TOP_LEFT_RECT)
            self._top_left_display = not self._top_left_display

        if self._counter % self._settings.TOP_RIGHT_FREQUENCY == 0:

            if self._top_right_display:
                self._display_surf.blit(self._top_right_surface_light, (self.TOP_RIGHT_RECT.x, self.TOP_RIGHT_RECT.y))
            else:
                self._display_surf.blit(self._top_right_surface_dark, (self.TOP_RIGHT_RECT.x, self.TOP_RIGHT_RECT.y))

            rects_to_be_rendered.append(self.TOP_RIGHT_RECT)
            self._top_right_display = not self._top_right_display

        if self._counter % self._settings.BOTTOM_LEFT_FREQUENCY == 0:

            if self._bottom_left_display:
                self._display_surf.blit(self._bottom_left_surface_light, (self.BOTTOM_LEFT_RECT.x, self.BOTTOM_LEFT_RECT.y))
            else:
                self._display_surf.blit(self._bottom_left_surface_dark, (self.BOTTOM_LEFT_RECT.x, self.BOTTOM_LEFT_RECT.y))

            rects_to_be_rendered.append(self.BOTTOM_LEFT_RECT)
            self._bottom_left_display = not self._bottom_left_display

        if self._counter % self._settings.BOTTOM_RIGHT_FREQUENCY == 0:

            if self._bottom_right_display:
                self._display_surf.blit(self._bottom_right_surface_light, (self.BOTTOM_RIGHT_RECT.x, self.BOTTOM_RIGHT_RECT.y))
            else:
                self._display_surf.blit(self._bottom_right_surface_dark, (self.BOTTOM_RIGHT_RECT.x, self.BOTTOM_RIGHT_RECT.y))

            rects_to_be_rendered.append(self.BOTTOM_RIGHT_RECT)
            self._bottom_right_display = not self._bottom_right_display

        pygame.display.update(rects_to_be_rendered)

    def on_cleanup(self):
        pygame.quit()

    def on_execute(self):
        if self.on_init() == False:
            self._running = False

        while(self._running):
            self._clock.tick_busy_loop(self._settings.FRAMES_PER_SECOND)
            # print(self._clock.tick_busy_loop(FRAMES_PER_SECOND))
            # print(self._clock.get_fps())
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()
        self.on_cleanup()


if __name__ == "__main__":
    theApp = App()
    theApp.on_execute()
