import pygame

pygame.font.init()
font_name = 'Arial'
font_4x4 = pygame.font.SysFont(font_name, 50)
font_2x2 = pygame.font.SysFont(font_name, 100)
font_1x1 = pygame.font.SysFont(font_name, 150)

class Chars:
    def __init__(self, settings):
        self._settings = settings
        self._all_chars = [
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
            'Q','R','S','T','U','V','W','X','Y','Z','.',',','"','\\','?','&',
            '1','2','3','4','5','6','7','8','9','0','-','+','*','/','^','=',
            '⌫','@','$','!','~','_','{','}','(',')','[',']','<','>','?','⏎'
        ]

        self._prerendered_4x4 = {}
        for ch in self._all_chars:
            self._prerendered_4x4[ch] = font_4x4.render(ch, False, settings.COLOR_WHITE)

        self._prerendered_2x2 = {}
        for ch in self._all_chars:
            self._prerendered_2x2[ch] = font_2x2.render(ch, False, settings.COLOR_WHITE)

        self._prerendered_1x1 = {}
        for ch in self._all_chars:
            self._prerendered_1x1[ch] = font_1x1.render(ch, False, settings.COLOR_WHITE)

        self._top_left_4x4 = [
            ['A', 'B', 'C', 'D'],
            ['E', 'F', 'G', 'H'],
            ['I', 'J', 'K', 'L'],
            ['M', 'N', 'O', 'P']] 

        self._top_right_4x4 = [
            ['Q', 'R', 'S', 'T'],
            ['U', 'V', 'W', 'X'],
            ['Y', 'Z', '.', ','],
            ['"', '\\', '?', '&']] 

        self._bottom_left_4x4 = [
            ['1', '2', '3', '4'],
            ['5', '6', '7', '8'],
            ['9', '0', '-', '+'],
            ['*', '/', '^', '=']] 

        self._bottom_right_4x4 = [
            ['⌫', '@', '$', '!'],
            ['~', '_', '{', '}'],
            ['(', ')', '[', ']'],
            ['<', '>', '?', '⏎']]       
