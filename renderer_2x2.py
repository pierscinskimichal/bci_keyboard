class Renderer_2x2:
    def __init__(self, chars, settings, matrix_4x4):
        
        self._charmap = {}
        self._charmap[0] = self.get_2x2_from(matrix_4x4, 0)
        self._charmap[1] = self.get_2x2_from(matrix_4x4, 1)
        self._charmap[2] = self.get_2x2_from(matrix_4x4, 2)
        self._charmap[3] = self.get_2x2_from(matrix_4x4, 3)

        self._prerendered_2x2 = chars._prerendered_2x2

        self._padding_left_2x2 = settings._padding_left_2x2
        self._padding_top_2x2 = settings._padding_top_2x2
        self._horizontal_gap_2x2 = settings._horizontal_gap_2x2
        self._vertical_gap_2x2 = settings._vertical_gap_2x2

    def fill_surface(self, surface, quarter):
        for row in range(0, 2):
            for column in range(0, 2):
                surface.blit(
                    self._prerendered_2x2[self._charmap[quarter][row][column]],
                    (
                        0 + self._padding_left_2x2 + column * self._horizontal_gap_2x2,
                        0 + self._padding_top_2x2 + row * self._vertical_gap_2x2
                    )
                )

    def get_chars_by_quarter(self, quarter):
        return self._charmap[quarter]

    def get_2x2_from(self, matrix_4x4, quarter):
        # TOP LEFT
        if quarter == 0: 
            return [ 
                [matrix_4x4[0][0], matrix_4x4[0][1]],
                [matrix_4x4[1][0], matrix_4x4[1][1]]
            ]
        # TOP RIGHT
        elif quarter == 1:
            return [ 
                [matrix_4x4[0][2], matrix_4x4[0][3]],
                [matrix_4x4[1][2], matrix_4x4[1][3]]
            ]
        # BOTTOM_LEFT
        elif quarter == 2:
            return [ 
                [matrix_4x4[2][0], matrix_4x4[2][1]],
                [matrix_4x4[3][0], matrix_4x4[3][1]]
            ]
        # BOTTOM_RIGHT
        elif quarter == 3:
            return [ 
                [matrix_4x4[2][2], matrix_4x4[2][3]],
                [matrix_4x4[3][2], matrix_4x4[3][3]]
            ]
        else:
            return []



